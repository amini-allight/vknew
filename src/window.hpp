#pragma once

#include "preamble.hpp"
#include "context.hpp"

class Window
{
public:
    Window();
    Window(const Window& rhs) = delete;
    Window(Window&& rhs) = delete;
    ~Window();

    Window& operator=(const Window& rhs) = delete;
    Window& operator=(Window&& rhs) = delete;

    void run();

    SDL_Window* window;

private:
    Context* ctx;
};
