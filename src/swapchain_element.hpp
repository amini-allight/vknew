#pragma once

#include "preamble.hpp"
#include "entity.hpp"

class Swapchain;
class Context;

class SwapchainElement
{
public:
    SwapchainElement(Swapchain* swapchain, VkImage image);
    SwapchainElement(const SwapchainElement& rhs) = delete;
    SwapchainElement(SwapchainElement&& rhs) = delete;
    ~SwapchainElement();

    SwapchainElement& operator=(const SwapchainElement& rhs) = delete;
    SwapchainElement& operator=(SwapchainElement&& rhs) = delete;

    void draw();

    Context* ctx;
    Swapchain* swapchain;

    VkImage image;
    VkImageView imageView;
    VkFramebuffer framebuffer;
    VkCommandBuffer commandBuffer;
    VkSemaphore startSemaphore;
    VkSemaphore endSemaphore;
    VkFence fence;
    VkFence lastFence = nullptr;

    VkDescriptorSet descriptorSet;
    int nextUniformIndex = 0;

    vector<Entity*> entities;

private:
    void prepareTexture();
    void imageToAttachmentLayout();
    void imageToPresentLayout();
};
