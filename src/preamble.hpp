#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include <vulkan/vulkan.h>
#include "vk_mem_alloc.h"

#include <cstdint>
#include <vector>
#include <set>
#include <string>
#include <iostream>
#include <fstream>
#include <limits>
#include <algorithm>

using namespace std;

#define CHECK_VK_RESULT(_expr) \
result = _expr; \
if (result < 0) \
{ \
    cerr << "Failure running '" << #_expr << "': " << result << endl; \
}

#define GET_EXTENSION_FUNCTION(_id) reinterpret_cast<PFN_##_id>(vkGetInstanceProcAddr(ctx->instance, #_id))

#pragma pack(1)
struct Vertex
{
    float x;
    float y;
    float z;
    float u;
    float v;
};
#pragma pack()

struct VmaBuffer
{
    VkBuffer buffer;
    VmaAllocation allocation;
};

struct VmaImage
{
    VkImage image;
    VmaAllocation allocation;
};

static const char* const programName = "vknew";
static const set<const char*> instanceExtensionNames = {
    "VK_KHR_surface",
    "VK_EXT_debug_utils"
};
static const set<const char*> deviceExtensionNames = {
    "VK_KHR_swapchain",
    "VK_EXT_shader_object"
};
static const char* const layerNames[] = {
    "VK_LAYER_KHRONOS_validation"
};
static const int majorVersion = 0;
static const int minorVersion = 1;
static const int patchVersion = 0;

static const int pushConstantRangeSize = 128;

static const int maxDescriptorSets = 10;
static const int maxDescriptorCount = 65536;

static const int defaultWindowWidth = 1600;
static const int defaultWindowHeight = 900;
