#include "window.hpp"

int main(int, char**)
{
    auto window = new Window();

    window->run();

    delete window;

    return 0;
}
