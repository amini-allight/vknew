#include "swapchain.hpp"
#include "context.hpp"

Swapchain::Swapchain(Context* ctx, unsigned width, unsigned height)
    : ctx(ctx)
    , width(width)
    , height(height)
    , currentFrame(0)
    , imageIndex(0)
{
    VkResult result;

    VkSurfaceCapabilitiesKHR capabilities;
    CHECK_VK_RESULT(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(ctx->physicalDevice, ctx->surface, &capabilities));

    width = clamp(width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
    height = clamp(height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);

    uint32_t formatCount;
    CHECK_VK_RESULT(vkGetPhysicalDeviceSurfaceFormatsKHR(ctx->physicalDevice, ctx->surface, &formatCount, nullptr));

    vector<VkSurfaceFormatKHR> formats(formatCount);
    CHECK_VK_RESULT(vkGetPhysicalDeviceSurfaceFormatsKHR(ctx->physicalDevice, ctx->surface, &formatCount, formats.data()));

    VkSurfaceFormatKHR chosenFormat = formats[0];

    for (const VkSurfaceFormatKHR& format : formats)
    {
        if (format.format == VK_FORMAT_B8G8R8A8_UNORM)
        {
            chosenFormat = format;
            break;
        }
    }

    format = chosenFormat.format;

    VkSwapchainCreateInfoKHR createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = ctx->surface;
    createInfo.minImageCount = capabilities.minImageCount;
    createInfo.imageFormat = chosenFormat.format;
    createInfo.imageColorSpace = chosenFormat.colorSpace;
    createInfo.imageExtent = { width, height };
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    createInfo.preTransform = capabilities.currentTransform;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode = VK_PRESENT_MODE_FIFO_KHR;
    createInfo.clipped = true;

    CHECK_VK_RESULT(vkCreateSwapchainKHR(ctx->device, &createInfo, nullptr, &swapchain));

    uint32_t imageCount;
    CHECK_VK_RESULT(vkGetSwapchainImagesKHR(ctx->device, swapchain, &imageCount, nullptr));

    vector<VkImage> images(imageCount);
    CHECK_VK_RESULT(vkGetSwapchainImagesKHR(ctx->device, swapchain, &imageCount, images.data()));

    for (VkImage image : images)
    {
        elements.push_back(new SwapchainElement(this, image));
    }
}

Swapchain::~Swapchain()
{
    for (SwapchainElement* element : elements)
    {
        delete element;
    }
    elements.clear();
    vkDestroySwapchainKHR(ctx->device, swapchain, nullptr);
}

bool Swapchain::draw()
{
    VkResult result;

    const SwapchainElement* currentElement = elements.at(currentFrame);

    CHECK_VK_RESULT(vkWaitForFences(ctx->device, 1, &currentElement->fence, true, numeric_limits<uint64_t>::max()));

    result = vkAcquireNextImageKHR(
        ctx->device,
        swapchain,
        numeric_limits<uint64_t>::max(),
        currentElement->startSemaphore,
        nullptr,
        &imageIndex
    );

    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
    {
        return true;
    }
    else if (result < 0)
    {
        cerr << "Failure running 'vkAcquireNextImageKHR': " << result << endl;
    }

    SwapchainElement* element = elements.at(imageIndex);

    if (element->lastFence)
    {
        CHECK_VK_RESULT(vkWaitForFences(ctx->device, 1, &element->lastFence, true, numeric_limits<uint64_t>::max()));
    }
    element->lastFence = currentElement->fence;

    CHECK_VK_RESULT(vkResetFences(ctx->device, 1, &currentElement->fence));

    element->draw();

    VkPipelineStageFlags waitStage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &currentElement->startSemaphore;
    submitInfo.pWaitDstStageMask = &waitStage;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &element->commandBuffer;
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &currentElement->endSemaphore;

    CHECK_VK_RESULT(vkQueueSubmit(ctx->queue, 1, &submitInfo, currentElement->fence));

    VkPresentInfoKHR presentInfo{};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = &currentElement->endSemaphore;
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = &swapchain;
    presentInfo.pImageIndices = &imageIndex;

    result = vkQueuePresentKHR(ctx->queue, &presentInfo);

    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
    {
        return true;
    }
    else if (result < 0)
    {
        cerr << "Failure running 'vkQueuePresentKHR': " << result << endl;
    }

    currentFrame = (currentFrame + 1) % elements.size();

    return false;
}
