#pragma once

#include "preamble.hpp"
#include "swapchain.hpp"

class Window;

class Context
{
public:
    Context(Window* window);
    Context(const Context& rhs) = delete;
    Context(Context&& rhs) = delete;
    ~Context();

    Context& operator=(const Context& rhs) = delete;
    Context& operator=(Context&& rhs) = delete;

    void draw();
    void resize();

    // General stuff, every renderer needs these
    VkInstance instance;
    VkDebugUtilsMessengerEXT debugMessenger;
    VkSurfaceKHR surface;
    uint32_t queueFamilyIndex;
    VkPhysicalDevice physicalDevice;
    VkDevice device;
    VmaAllocator memoryAllocator;
    VkQueue queue;
    VkCommandPool commandPool;
    VkDescriptorPool descriptorPool;
    VkDescriptorSetLayout descriptorSetLayout;
    VkPushConstantRange pushConstantRange;
    VkPipelineLayout pipelineLayout;
    VkSampler sampler;
    // Specific stuff, for the object we will be rendering
    VkShaderEXT shaders[2];
    VmaBuffer mesh;
    VmaImage texture;
    VkImageView textureView;
    bool textureReady = false;

private:
    Window* window;
    // To make extension function macro work
    Context* ctx;
    Swapchain* swapchain;

    void createInstance(SDL_Window* window);
    void createDebugUtilsMessenger();
    void createDevice();
    void createAllocator();
    void createCommandPool();
    void createDescriptorPool();
    void createDescriptorSetLayout();
    void createPushConstantRange();
    void createPipelineLayout();
    void createSampler();
    void createShaders();
    void createMesh();
    void createTexture();
    void createTextureView();
    void prepareTexture(VkCommandBuffer commandBuffer);

    static VkBool32 onError(
        VkDebugUtilsMessageSeverityFlagBitsEXT severity,
        VkDebugUtilsMessageTypeFlagsEXT type,
        const VkDebugUtilsMessengerCallbackDataEXT* callbackData,
        void* userData
    );
};
