#include "window.hpp"

Window::Window()
{
    SDL_Init(SDL_INIT_VIDEO);

    window = SDL_CreateWindow(
        programName,
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        defaultWindowWidth,
        defaultWindowHeight,
        SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_VULKAN
    );

    if (!window)
    {
        cerr << "Failure creating SDL window: " << SDL_GetError() << endl;
    }

    ctx = new Context(this);
}

Window::~Window()
{
    delete ctx;
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void Window::run()
{
    while (true)
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT :
                return;
            case SDL_WINDOWEVENT :
                switch (event.window.event)
                {
                case SDL_WINDOWEVENT_SIZE_CHANGED :
                    ctx->resize();
                    break;
                }
            }
        }

        ctx->draw();
    }
}
