#pragma once

#include "preamble.hpp"
#include "swapchain_element.hpp"

class Context;

class Swapchain
{
public:
    Swapchain(Context* ctx, unsigned width, unsigned height);
    Swapchain(const Swapchain& rhs) = delete;
    Swapchain(Swapchain&& rhs) = delete;
    ~Swapchain();

    Swapchain& operator=(const Swapchain& rhs) = delete;
    Swapchain& operator=(Swapchain&& rhs) = delete;

    bool draw();

    Context* ctx;

    VkSwapchainKHR swapchain;
    VkFormat format;
    unsigned width;
    unsigned height;

private:
    uint32_t currentFrame;
    uint32_t imageIndex;
    vector<SwapchainElement*> elements;
};
