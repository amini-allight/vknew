#include "context.hpp"
#include "window.hpp"

static string getFile(const string& path)
{
    ifstream file(path);

    if (!file.good())
    {
        cerr << "Failed to read file: " << path << endl;
        return "";
    }
    
    return string(istreambuf_iterator<char>(file), istreambuf_iterator<char>());
}

Context::Context(Window* window)
    : window(window)
    , ctx(this)
{
    createInstance(window->window);

    createDebugUtilsMessenger();

    bool success = SDL_Vulkan_CreateSurface(window->window, instance, &surface);

    if (!success)
    {
        cerr << "Failure creating SDL Vulkan surface: " << SDL_GetError() << endl;
    }

    createDevice();

    createAllocator();

    createCommandPool();

    createDescriptorPool();

    createDescriptorSetLayout();

    createPushConstantRange();

    createPipelineLayout();

    createSampler();

    createShaders();

    createMesh();

    createTexture();

    createTextureView();

    swapchain = new Swapchain(this, defaultWindowWidth, defaultWindowHeight);
}

Context::~Context()
{
    VkResult result;

    CHECK_VK_RESULT(vkDeviceWaitIdle(device));

    delete swapchain;

    vkDestroyImageView(device, textureView, nullptr);
    vmaDestroyImage(memoryAllocator, texture.image, texture.allocation);
    vmaDestroyBuffer(memoryAllocator, mesh.buffer, mesh.allocation);
    for (VkShaderEXT shader : shaders)
    {
        GET_EXTENSION_FUNCTION(vkDestroyShaderEXT)(device, shader, nullptr);
    }
    vkDestroySampler(device, sampler, nullptr);
    vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
    vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);
    vkDestroyDescriptorPool(device, descriptorPool, nullptr);
    vkDestroyCommandPool(device, commandPool, nullptr);
    vmaDestroyAllocator(memoryAllocator);
    vkDestroyDevice(device, nullptr);
    vkDestroySurfaceKHR(instance, surface, nullptr);
    GET_EXTENSION_FUNCTION(vkDestroyDebugUtilsMessengerEXT)(instance, debugMessenger, nullptr);
    vkDestroyInstance(instance, nullptr);
}

void Context::draw()
{
    bool shouldResize = swapchain->draw();

    if (shouldResize)
    {
        resize();
    }
}

void Context::resize()
{
    VkResult result;

    CHECK_VK_RESULT(vkDeviceWaitIdle(device));

    int width;
    int height;
    SDL_GetWindowSize(window->window, &width, &height);

    delete swapchain;
    swapchain = new Swapchain(this, width, height);
}

void Context::createInstance(SDL_Window* window)
{
    VkResult result;

    set<const char*> extensionNames = instanceExtensionNames;

    uint32_t windowingExtensionNameCount;
    bool success = SDL_Vulkan_GetInstanceExtensions(window, &windowingExtensionNameCount, nullptr);

    if (!success)
    {
        cerr << "Failure getting SDL Vulkan instance extensions: " << SDL_GetError() << endl;
    }

    vector<const char*> windowingExtensionNames(windowingExtensionNameCount);
    success = SDL_Vulkan_GetInstanceExtensions(window, &windowingExtensionNameCount, windowingExtensionNames.data());

    if (!success)
    {
        cerr << "Failure getting SDL Vulkan instance extensions: " << SDL_GetError() << endl;
    }

    extensionNames.insert(windowingExtensionNames.begin(), windowingExtensionNames.end());

    vector<const char*> linearExtensionNames = { extensionNames.begin(), extensionNames.end() };

    VkApplicationInfo applicationInfo{};
    applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    applicationInfo.pApplicationName = programName;
    applicationInfo.applicationVersion = VK_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
    applicationInfo.pEngineName = programName;
    applicationInfo.engineVersion = VK_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
    applicationInfo.apiVersion = VK_API_VERSION_1_3;

    VkInstanceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &applicationInfo;
    createInfo.enabledExtensionCount = linearExtensionNames.size();
    createInfo.ppEnabledExtensionNames = linearExtensionNames.data();

    uint32_t instanceLayerCount;
    CHECK_VK_RESULT(vkEnumerateInstanceLayerProperties(&instanceLayerCount, nullptr));

    vector<VkLayerProperties> layerProperties(instanceLayerCount);
    CHECK_VK_RESULT(vkEnumerateInstanceLayerProperties(&instanceLayerCount, layerProperties.data()));

    size_t foundLayers = 0;

    for (const VkLayerProperties& layer : layerProperties)
    {
        for (size_t i = 0; i < sizeof(layerNames) / sizeof(const char*); i++)
        {
            if (string(layer.layerName) == string(layerNames[i]))
            {
                foundLayers++;
            }
        }
    }

    if (foundLayers >= sizeof(layerNames) / sizeof(const char*))
    {
        createInfo.enabledLayerCount = sizeof(layerNames) / sizeof(const char*);
        createInfo.ppEnabledLayerNames = layerNames;
    }

    CHECK_VK_RESULT(vkCreateInstance(&createInfo, nullptr, &instance));
}

void Context::createDebugUtilsMessenger()
{
    VkResult result;

    VkDebugUtilsMessengerCreateInfoEXT createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    createInfo.pfnUserCallback = onError;

    CHECK_VK_RESULT(GET_EXTENSION_FUNCTION(vkCreateDebugUtilsMessengerEXT)(instance, &createInfo, nullptr, &debugMessenger));
}

void Context::createDevice()
{
    VkResult result;

    uint32_t physicalDeviceCount;
    CHECK_VK_RESULT(vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, nullptr));

    vector<VkPhysicalDevice> physicalDevices(physicalDeviceCount);
    CHECK_VK_RESULT(vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, physicalDevices.data()));

    uint32_t bestScore = 0;

    for (VkPhysicalDevice physicalDevice : physicalDevices)
    {
        VkPhysicalDeviceProperties properties{};
        vkGetPhysicalDeviceProperties(physicalDevice, &properties);

        uint32_t score;

        switch (properties.deviceType)
        {
        default :
            continue;
        case VK_PHYSICAL_DEVICE_TYPE_OTHER :
            score = 1;
            break;
        case VK_PHYSICAL_DEVICE_TYPE_CPU :
            score = 2;
            break;
        case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU :
            score = 3;
            break;
        case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU :
            score = 4;
            break;
        case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU :
            score = 5;
            break;
        }

        if (score > bestScore)
        {
            this->physicalDevice = physicalDevice;
            bestScore = score;
        }
    }

    {
        uint32_t queueFamilyCount;
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);

        vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies.data());

        queueFamilyIndex = numeric_limits<uint32_t>::max();

        for (uint32_t i = 0; i < queueFamilyCount; i++)
        {
            VkBool32 present = false;

            CHECK_VK_RESULT(vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface, &present));

            if (present && (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT))
            {
                queueFamilyIndex = i;
            }
        }
    }

    {
        vector<const char*> linearExtensionNames = { deviceExtensionNames.begin(), deviceExtensionNames.end() };

        float priority = 1;

        VkDeviceQueueCreateInfo queueCreateInfo{};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = queueFamilyIndex;
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &priority;

        VkPhysicalDeviceDescriptorIndexingFeatures descriptorSetIndexingFeatures{};
        descriptorSetIndexingFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES;
        descriptorSetIndexingFeatures.descriptorBindingPartiallyBound = true;
        descriptorSetIndexingFeatures.runtimeDescriptorArray = true;

        VkPhysicalDeviceShaderObjectFeaturesEXT shaderObjectFeatures{};
        shaderObjectFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_OBJECT_FEATURES_EXT;
        shaderObjectFeatures.pNext = &descriptorSetIndexingFeatures;
        shaderObjectFeatures.shaderObject = true;

        VkPhysicalDeviceDynamicRenderingFeatures dynamicRenderingFeatures{};
        dynamicRenderingFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES;
        dynamicRenderingFeatures.pNext = &shaderObjectFeatures;
        dynamicRenderingFeatures.dynamicRendering = true;

        VkDeviceCreateInfo createInfo{};
        createInfo.pNext = &dynamicRenderingFeatures;
        createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        createInfo.queueCreateInfoCount = 1;
        createInfo.pQueueCreateInfos = &queueCreateInfo;
        createInfo.enabledExtensionCount = linearExtensionNames.size();
        createInfo.ppEnabledExtensionNames = linearExtensionNames.data();

        uint32_t deviceLayerCount;
        CHECK_VK_RESULT(vkEnumerateDeviceLayerProperties(physicalDevice, &deviceLayerCount, nullptr));

        vector<VkLayerProperties> layerProperties(deviceLayerCount);
        CHECK_VK_RESULT(vkEnumerateDeviceLayerProperties(physicalDevice, &deviceLayerCount, layerProperties.data()));

        size_t foundLayers = 0;

        for (const VkLayerProperties& layer : layerProperties)
        {
            for (size_t i = 0; i < sizeof(layerNames) / sizeof(const char*); i++)
            {
                if (string(layer.layerName) == string(layerNames[i]))
                {
                    foundLayers++;
                }
            }
        }

        if (foundLayers >= sizeof(layerNames) / sizeof(const char*))
        {
            createInfo.enabledLayerCount = sizeof(layerNames) / sizeof(const char*);
            createInfo.ppEnabledLayerNames = layerNames;
        }

        CHECK_VK_RESULT(vkCreateDevice(physicalDevice, &createInfo, nullptr, &device));

        vkGetDeviceQueue(device, queueFamilyIndex, 0, &queue);
    }
}

void Context::createAllocator()
{
    VkResult result;

    VmaAllocatorCreateInfo createInfo{};
    createInfo.physicalDevice = physicalDevice;
    createInfo.device = device;
    createInfo.instance = instance;

    CHECK_VK_RESULT(vmaCreateAllocator(&createInfo, &memoryAllocator));
}

void Context::createCommandPool()
{
    VkResult result;

    VkCommandPoolCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    createInfo.queueFamilyIndex = queueFamilyIndex;
    createInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    CHECK_VK_RESULT(vkCreateCommandPool(device, &createInfo, nullptr, &commandPool));
}

void Context::createDescriptorPool()
{
    VkResult result;

    VkDescriptorPoolSize poolSizes[] = {
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, maxDescriptorSets * maxDescriptorCount },
        { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, maxDescriptorSets * maxDescriptorCount }
    };

    VkDescriptorPoolCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    createInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    createInfo.maxSets = maxDescriptorSets;
    createInfo.poolSizeCount = sizeof(poolSizes) / sizeof(VkDescriptorPoolSize);
    createInfo.pPoolSizes = poolSizes;

    CHECK_VK_RESULT(vkCreateDescriptorPool(device, &createInfo, nullptr, &descriptorPool));
}

void Context::createDescriptorSetLayout()
{
    VkResult result;

    VkDescriptorSetLayoutBinding bindings[2]{};
    bindings[0].binding = 0;
    bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    bindings[0].descriptorCount = maxDescriptorCount;
    bindings[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;

    bindings[1].binding = 1;
    bindings[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    bindings[1].descriptorCount = maxDescriptorCount;
    bindings[1].stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;

    VkDescriptorBindingFlags bindingFlags[] = {
        VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT,
        VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT
    };

    VkDescriptorSetLayoutBindingFlagsCreateInfo bindingFlagsCreateInfo{};
    bindingFlagsCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO;
    bindingFlagsCreateInfo.pBindingFlags = bindingFlags;
    bindingFlagsCreateInfo.bindingCount = sizeof(bindingFlags) / sizeof(VkDescriptorBindingFlags);

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.pNext = &bindingFlagsCreateInfo;
    createInfo.bindingCount = sizeof(bindings) / sizeof(VkDescriptorSetLayoutBinding);
    createInfo.pBindings = bindings;

    CHECK_VK_RESULT(vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout));
}

void Context::createPushConstantRange()
{
    pushConstantRange = VkPushConstantRange{};
    pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
    pushConstantRange.size = pushConstantRangeSize;
}

void Context::createPipelineLayout()
{
    VkResult result;

    VkPipelineLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    createInfo.setLayoutCount = 1;
    createInfo.pSetLayouts = &descriptorSetLayout;
    createInfo.pushConstantRangeCount = 1;
    createInfo.pPushConstantRanges = &pushConstantRange;

    CHECK_VK_RESULT(vkCreatePipelineLayout(device, &createInfo, nullptr, &pipelineLayout));
}

void Context::createSampler()
{
    VkResult result;

    VkSamplerCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    createInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    createInfo.maxAnisotropy = 16;
    createInfo.maxLod = VK_LOD_CLAMP_NONE;
    createInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;

    CHECK_VK_RESULT(vkCreateSampler(device, &createInfo, nullptr, &sampler));
}

void Context::createShaders()
{
    VkResult result;

    string vertexSPIRV = getFile("vertex.spv");
    string fragmentSPIRV = getFile("fragment.spv");

    VkShaderCreateInfoEXT createInfos[2]{};

    createInfos[0].sType = VK_STRUCTURE_TYPE_SHADER_CREATE_INFO_EXT;
    createInfos[0].flags = VK_SHADER_CREATE_LINK_STAGE_BIT_EXT;
    createInfos[0].codeType = VK_SHADER_CODE_TYPE_SPIRV_EXT;
    createInfos[0].pName = "main";
    createInfos[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
    createInfos[0].nextStage = VK_SHADER_STAGE_FRAGMENT_BIT;
    createInfos[0].codeSize = vertexSPIRV.size();
    createInfos[0].pCode = vertexSPIRV.data();
    createInfos[0].setLayoutCount = 1;
    createInfos[0].pSetLayouts = &descriptorSetLayout;
    createInfos[0].pushConstantRangeCount = 1;
    createInfos[0].pPushConstantRanges = &pushConstantRange;

    createInfos[1].sType = VK_STRUCTURE_TYPE_SHADER_CREATE_INFO_EXT;
    createInfos[1].flags = VK_SHADER_CREATE_LINK_STAGE_BIT_EXT;
    createInfos[1].codeType = VK_SHADER_CODE_TYPE_SPIRV_EXT;
    createInfos[1].pName = "main";
    createInfos[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    createInfos[1].codeSize = fragmentSPIRV.size();
    createInfos[1].pCode = fragmentSPIRV.data();
    createInfos[1].setLayoutCount = 1;
    createInfos[1].pSetLayouts = &descriptorSetLayout;
    createInfos[1].pushConstantRangeCount = 1;
    createInfos[1].pPushConstantRanges = &pushConstantRange;

    CHECK_VK_RESULT(GET_EXTENSION_FUNCTION(vkCreateShadersEXT)(
        device,
        sizeof(createInfos) / sizeof(VkShaderCreateInfoEXT),
        createInfos,
        nullptr,
        shaders
    ));
}

void Context::createMesh()
{
    VkResult result;

    VkBufferCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    createInfo.size = 4 * sizeof(Vertex);
    createInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
    createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VmaAllocationCreateInfo allocInfo{};
    allocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

    CHECK_VK_RESULT(vmaCreateBuffer(
        memoryAllocator,
        &createInfo,
        &allocInfo,
        &mesh.buffer,
        &mesh.allocation,
        nullptr
    ));

    Vertex* vertices = nullptr;
    CHECK_VK_RESULT(vmaMapMemory(memoryAllocator, mesh.allocation, reinterpret_cast<void**>(&vertices)));

    vertices[0].x = -1;
    vertices[0].y = -1;
    vertices[0].z = 0;
    vertices[0].u = 0;
    vertices[0].v = 0;

    vertices[1].x = +1;
    vertices[1].y = -1;
    vertices[1].z = 0;
    vertices[1].u = 1;
    vertices[1].v = 0;

    vertices[2].x = -1;
    vertices[2].y = +1;
    vertices[2].z = 0;
    vertices[2].u = 0;
    vertices[2].v = 1;

    vertices[3].x = +1;
    vertices[3].y = +1;
    vertices[3].z = 0;
    vertices[3].u = 1;
    vertices[3].v = 1;

    vmaUnmapMemory(memoryAllocator, mesh.allocation);
}

void Context::createTexture()
{
    VkResult result;

    static const int textureWidth = 8;
    static const int textureHeight = 8;

    vector<uint32_t> texturePixels(32 * 32, 0xffffffff);

    for (int y = 0; y < textureHeight; y++)
    {
        for (int x = 0; x < textureWidth; x++)
        {
            if ((y % 2 == 0 && x % 2 == 0) || (y % 2 == 1 && x % 2 == 1))
            {
                texturePixels[y * textureWidth + x] = 0xff0000ff;
            }
        }
    }

    VkImageCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    createInfo.imageType = VK_IMAGE_TYPE_2D;
    createInfo.extent = { textureWidth, textureHeight, 1 };
    createInfo.mipLevels = 1;
    createInfo.arrayLayers = 1;
    createInfo.format = VK_FORMAT_B8G8R8A8_SRGB;
    createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    createInfo.tiling = VK_IMAGE_TILING_LINEAR;
    createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    createInfo.initialLayout = VK_IMAGE_LAYOUT_PREINITIALIZED;
    createInfo.usage = VK_IMAGE_USAGE_SAMPLED_BIT;

    VmaAllocationCreateInfo allocInfo{};
    allocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

    CHECK_VK_RESULT(vmaCreateImage(
        memoryAllocator,
        &createInfo,
        &allocInfo,
        &texture.image,
        &texture.allocation,
        nullptr
    ));

    VkImageSubresource subresource{};
    subresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    VkSubresourceLayout layout{};
    vkGetImageSubresourceLayout(device, texture.image, &subresource, &layout);

    int cpuPitch = textureWidth;
    int gpuPitch = layout.rowPitch / sizeof(uint32_t);

    uint32_t* cpuPixels = texturePixels.data();
    uint32_t* gpuPixels = nullptr;
    CHECK_VK_RESULT(vmaMapMemory(memoryAllocator, texture.allocation, reinterpret_cast<void**>(&gpuPixels)));

    for (int y = 0; y < textureHeight; y++)
    {
        for (int x = 0; x < textureWidth; x++)
        {
            gpuPixels[y * gpuPitch + x] = cpuPixels[y * cpuPitch + x];
        }
    }

    vmaUnmapMemory(memoryAllocator, texture.allocation);
}

void Context::createTextureView()
{
    VkResult result;

    VkImageViewCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.subresourceRange.baseMipLevel = 0;
    createInfo.subresourceRange.levelCount = VK_REMAINING_MIP_LEVELS;
    createInfo.subresourceRange.baseArrayLayer = 0;
    createInfo.subresourceRange.layerCount = VK_REMAINING_ARRAY_LAYERS;
    createInfo.image = texture.image;
    createInfo.format = VK_FORMAT_B8G8R8A8_SRGB;
    createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

    CHECK_VK_RESULT(vkCreateImageView(device, &createInfo, nullptr, &textureView));
}

VkBool32 Context::onError(
    VkDebugUtilsMessageSeverityFlagBitsEXT severity,
    VkDebugUtilsMessageTypeFlagsEXT type,
    const VkDebugUtilsMessengerCallbackDataEXT* callbackData,
    void* userData
)
{
    string message = "Vulkan ";

    switch (type)
    {
    case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT :
        message += "general ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT :
        message += "validation ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT :
        message += "performance ";
        break;
    }

    switch (severity)
    {
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT :
        message += "(verbose): ";
        break;
    default :
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT :
        message += "(info): ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT :
        message += "(warning): ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT :
        message += "(error): ";
        break;
    }

    message += callbackData->pMessage;

    switch (severity)
    {
    default :
        cout << message << endl;
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT :
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT :
        cerr << message << endl;
        break;
    }

    return false;
}
