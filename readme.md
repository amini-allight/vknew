# vknew: Modern Vulkan with descriptor indexing, dynamic rendering and shader objects

This is the source code for my [modern Vulkan with descriptor indexing, dynamic rendering and shader objects tutorial](https://amini-allight.org/post/vknew-modern-vulkan-with-descriptor-indexing-dynamic-rendering-and-shader-objects).

## Dependencies

- glslangValidator (only required for build)
- SDL2
- Vulkan (version 1.3+ with the `VK_EXT_shader_object` extension)

## Usage

Build the application with:

```sh
mkdir -p build
cd build
cmake ..
make -j$(nproc)
```

Then run it with:

```sh
cd bin
./vknew
```

A window should appear featuring a number of checkered squares.

## License

Created by Amini Allight. The contents of this repository are licensed under Creative Commons Zero (CC0 1.0), placing them in the public domain.

The one exception to this is the file `vk_mem_alloc.h` taken from AMD's [Vulkan Memory Allocator](https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator) project, the license details of which can be found in the header of that same file.

This tutorial is not affiliated with or endorsed by the Khronos Group.
